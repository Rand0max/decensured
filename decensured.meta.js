// ==UserScript==
// @name        Décensured
// @namespace   decensuredjvcom
// @version     1.3.1
// @downloadURL https://github.com/Rand0max/decensured/raw/master/decensured.user.js
// @updateURL   https://github.com/Rand0max/decensured/raw/master/decensured.meta.js
// @author      Rand0max
// @description Contrer la censure et le 410 sur les forums JVC
// @icon        https://risibank.fr/cache/medias/0/2/209/20968/thumb.png
// @match       http://www.jeuxvideo.com/forums/*
// @match       https://www.jeuxvideo.com/forums/*
// @match       http://www.jeuxvideo.com/recherche/forums/*
// @match       https://www.jeuxvideo.com/recherche/forums/*
// @match       http://www.jeuxvideo.com/profil/*
// @match       https://www.jeuxvideo.com/profil/*
// @connect     myexternalip.com
// @connect     noelshack.com
// @connect     hastebin.com
// @connect     deboucled.jvflux.fr
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addStyle
// @grant       GM_deleteValue
// @grant       GM_listValues
// @grant       GM_getResourceText
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.addStyle
// @grant       GM.deleteValue
// @grant       GM.listValues
// @grant       GM.getResourceText
// @grant       GM.xmlHttpRequest
// @resource    DECENSURED_CSS https://raw.githubusercontent.com/Rand0max/decensured/master/decensured.css
// @require     https://unpkg.com/gm-storage@2.0.3/dist/index.umd.min.js
// @require     https://unpkg.com/timespan@2.3.0/browser/TimeSpan-1.2.min.js
// @run-at      document-end
// ==/UserScript==

