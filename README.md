# **Décensured**

## <i><b>Nouvelle version</b> - Webedia en PLS.</i>

<br>

> par le créateur du script [**Déboucled**](https://github.com/Rand0max/deboucled#readme)

# Résumé

## Dissimule vos messages afin d'éviter la censure abusive sur les forums JVC.

<br>

#### _Vous n'en pouvez plus de vous faire censurer sans raison ?_

#### _Vous n'en pouvez plus de cette "erreur 410" ?_

#### _Vous voulez discuter librement tout en restant sur le 18-25 ?_

**Alors ce script est fait pour vous !**

<br>

# Aperçu

<figure style="display:inline-block"><figcaption>Création d'un topic</figcaption><img src="https://i.imgur.com/9wV8VOI.png" alt="Décensured1" width="200"/></figure>

<figure style="display:inline-block"><figcaption>Topic censuré</figcaption><img src="https://i.imgur.com/F7hPjdZ.png" alt="Décensured2" width="200"/></figure>

<figure style="display:inline-block"><figcaption>Topic décensuré</figcaption><img src="https://i.imgur.com/UCQQ9DS.png" alt="Décensured3" width="200"/></figure>

> [Démonstration](https://i.imgur.com/PAuas65.gif)

<br>

# Installation

### Étape pour _Android_ : Il faut d'abord installer le navigateur **Kiwi Browser** (équivalent de Chrome)

<a href="https://play.google.com/store/apps/details?id=com.kiwibrowser.browser" target="_blank"><img src="https://kiwibrowser.com/wp-content/uploads/2019/09/cropped-Favicon-512x512-32x32.png" alt="Chrome" width="20"/> Kiwi Browser</a>

### Étape pour _iPhone_ : Il faut d'abord installer le navigateur **Insight Browser** (équivalent de Safari)

<a href="https://apps.apple.com/app/apple-store/id1531407280?mt=8" target="_blank"><img src="https://cdn.umso.co/pxvr5mgeg4se/assets/aum444nl.png" alt="Insight" width="20"/> Insight Browser</a>

### Étape 1 : Installez un gestionnaire d'userscript comme **TamperMonkey**

<a href="https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=fr" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/e/e1/Google_Chrome_icon_%28February_2022%29.svg" alt="Chrome" width="20"/> Chrome, Brave et Mobile (Kiwi)</a>

<a href="https://addons.mozilla.org/fr/firefox/addon/tampermonkey/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/a/a0/Firefox_logo%2C_2019.svg" alt="Firefox" width="20"/> Firefox</a>

<a href="https://addons.opera.com/fr/extensions/details/tampermonkey-beta/?display=en/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Opera_2015_icon.svg" alt="Opera" width="20"/> Opera</a>

<a href="https://apps.apple.com/app/apple-store/id1482490089?pt=117945903&ct=tm.net&mt=8/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Safari_browser_logo.svg/234px-Safari_browser_logo.svg.png" alt="Safari" width="20"/> Safari (Insight)</a>

<a href="https://microsoftedge.microsoft.com/addons/detail/tampermonkey/iikmkjmpaadaobahmlepeloendndfphd/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/9/98/Microsoft_Edge_logo_%282019%29.svg" alt="Edge" width="20"/> Edge</a>

### Étape 2 : Installez **Décensured**

- Installez **Décensured** en [cliquant ici](https://github.com/Rand0max/decensured/raw/master/decensured.user.js) ou [ici](https://jvscript.fr/script/decensured) puis cliquez sur le bouton "Installer" dans la fenêtre qui s'ouvre

# Mode d'emploi

- Après installation, écrivez votre message normalement.
- Cliquez sur le bouton bleu **'POST DÉCENSURED'** à la place du bouton "POSTER".
- Votre message apparaitra en bleu et décensuré pour vous et les utilisateurs du script.
- Les autres utilisateurs (y compris la modération) ne verront que des messages sans intérêt et générés aléatoirement.
- Vous pouvez aussi personnaliser vos messages et titres de "camouflages".

# F.A.Q

<details>
  <summary><b><i>Comment est-on protégés des modérateurs, des admins et des bots ?</i></b></summary>
<br>
<li>Seuls les utilisateurs du script voient vos messages.</li>
<li>Les autres, y compris la modération/administrateur/bot, verront des messages sans intérêt, ou personnalisés par vous-même.</li>
<li>Webedia est bloqué par diverses techniques et leurs modérateurs ne peuvent pas utiliser le script.</li>
<li>Les utilisateurs du script ne peuvent pas DDB vos messages masqués.</li>
<li>Les messages ne sont pas stockés sur les serveurs de webedia.</li>
<li>Les messages sont cryptés.</li>
<li>Les messages cryptés disparaitront automatiquement passé un délai, garantissant que personne ne puisse les relire plus tard.</li>
</details>

<br>

<details>
  <summary><b><i>Pourquoi ai-je été banni ou kick en utilisant le script ?</i></b></summary>
<br>
<li>Car certains admins ont décidé de bannir sans aucun motif tous les messages et topics qu'ils soupçonnent d'utiliser le script. Ils savent très bien que ça peut tomber sur quelqu'un qui n'est pas concerné, mais vous savez aussi bien que moi qu'il n'est plus possible de discuter d'un sujet aussi innocent soit-il sans risquer un ban ou un 410, script ou non. D'où l'existence de Décensured.</li>
</details>

<br>

<details>
  <summary><b><i>Alors comment éviter d'être ban ?</i></b></summary>
<br>
<li>Personnalisez les messages de "camouflage" avant de poster avec Décensured.</li>
<li>Participez un peu au topic de manière innocente.</li>
<li>Gardez bien le script à jour avec la dernière version, j'améliore régulièrement la technique pour être de plus en plus indétectable.</li>
<li>Ne parlez pas trop ouvertement du script, diffusez plutôt l'information par message privé.</li>
<li>Discutez sur des gros topics déjà modérés pour passer inaperçu.</li>
</details>

<br>

<details>
  <summary><b><i>Pourquoi le code du script est illisible et non open source ?</i></b></summary>
<br>
<li>Pour garantir une sécurité maximum aux utilisateurs et contrer l'offensive de Webedia, le code a été rendu illisible. Cela complique la tâche aux personnes mal intentionnées qui souhaiteraient bloquer l'utilisation du script.</li>
</details>

<br>

<details>
  <summary><b><i>Comment savoir si tu ne fais pas de carabistouilles avec le script ?</i></b></summary>
<br>
<li>Ceux qui ont suivi l'aventure Déboucled sur le topic officiel savent que je suis resté à l'écoute de la communauté, et que je n'ai jamais introduit la moindre ligne de code malveillante dans mes outils.</li>
<li>De plus, les userscripts sont exécutés dans un environnement isolé du reste de votre navigateur/appareil.</li>
<li>Enfin, je n'ai que faire de connaitre vos secondaires ou de savoir si vous utilisez un Xiaomi (chaud) ou un iPhone.</li>
<details>
  <summary><b><i>+ preuve en dépit des rageux</i></b></summary>
<br>
<img src="https://image.noelshack.com/fichiers/2017/07/1487361349-nofake2.png" width="20%"></img>
</details>
</details>

<br>

<details>
  <summary><b><i>Pourquoi je vois myexternalip dans l'entête du script ?</i></b></summary>
<br>
<li>Pour bloquer Webedia et ses admins, le script vérifie que l'adresse IP de l'utilisateur n'appartient pas à la « plage IP » allouée au réseau Webedia. Votre adresse IP n'est envoyée nul part, et n'est stockée nul part non plus. Cette vérification n'est faite qu'une fois, au premier lancement du script. <b>Contactez-moi si vous voulez voir un extrait du code.<b></li>
</details>

<br>

<details>
  <summary><b><i>Peut-on aider ? Partager des idées, des suggestions, des bugs ?</i></b></summary>
<br>
<li>La meilleure manière d'aider est de partager le script et de diffuser le lien de cette page.</li>
<li>Vous pouvez aussi créer un ticket <a href="https://github.com/Rand0max/decensured/issues">ici</a> si vous le souhaitez.</li>
</details>

<br>

<details>
  <summary><b><i>Comment te contacter ?</i></b></summary>
<br>
<li>Par email <a href="mailto:rand0max@protonmail.com" target="_blank">rand0max@protonmail.com</a></li>
<li>Sur Discord <a href="https://discord.com/users/781564172483166268" target="_blank">Rand0max#3135</a></li>
</details>

<br>

<details>
  <summary><b><i>N.B : à l'attention de Webedia et ses sbires</i></b></summary>
<br>
<li>Pas la peine de vous fatiguer les branques, y'aura toujours une parade. Paix sur vous nonobstant.</li>
</details>

<br>

## **_Rand0max_**
